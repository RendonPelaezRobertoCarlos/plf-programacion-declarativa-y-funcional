# TEMA: "Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)"
```plantuml
@startmindmap
*[#lightblue] Programación Declarativa
	*[#Green] Es un estilo de programación, un paradigma\nqué surge como reacción algunos problemas.
		*[#Orange] Tambien llamada
			* Programación perezosa
			* Programación cómoda
			* Programación hermosa
		*[#Orange] Consiste
			* Las tareas rutinarias de programación se dejan al compilador
		*[#Orange] Proposito
			* Conseguir reducir la complejidad de los programas
			* Conseguir reducir el riesgo de cometer\n errores haciendo programas 
			* Conseguir reducir la faragosidad del codigo
		*[#Orange] Pretende
			* Liberarse de las asignaciones
			* Liberarse de tener que detallar cuál es el control\n de la gestión de memoria en el ordenador
			* Utilizar otros recursos expresivos que permiten especificar\n los programas a un nivel más alto y cercano
		*[#Orange] Historia
			* A los ordenadores se les tenía que hablar en código máquina
			* Se desarrollan lenguajes de alto nivel
				* Fortran
		*[#Orange] Ventaja
			* Programas mas cortos
			* Faciles de realizar
			* Faciles de depurar
		*[#Orange] Problemas 
			* La idea es abstracta
			* Cuando uno sele un poco y Escoge el nivel adecuado\n de interacción con la máquina, puede hacer que la eficiencia\n sea igual que cuando se le está diciendo directamente\n a la máquina lo que tiene que hacer
			* Ordenacion
				* Problema crucial para resolver de manera muy eficiente
				* Es crucial cada paso, cada asignación de memoria
				* El compilador Toma las decisiones de manera automática\n y es muy difícil que sean eficientes
	*[#Green] Programacion Imperativa
		*[#Orange] Se basa en el modelo de Von Neumann
			* Se tienen que buscar alternativas expresivas\n para separarnos de este modelo
			* Según el tipo de lenguaje que se utilize
		*[#Orange] C, Modular, Pascal
		*[#Orange] Tiene cuello de botella
		*[#Orange] El programador se vio obligado a dar muchos\n detalles sobre los cálculos que se quieren realizar
	*[#Green] Programacion Funcional
		*[#Orange] Recurre al lenguaje que utilizan los matemáticos,\n en particular cuando describen funciones
		*[#Orange] Datos de entrada
			* Argumentos
		*[#Orange] Datos de salida
			* Es el resultado de la función
		*[#Orange] Ventajas
			* Funciones de orden superior
				* Una vez que tienes como objetos principales\n sus lenguajes las funciones puedes definir funciones\n que a su vez actúen como funciones.
				* Capacidad muy potente
		*[#Orange] Caracteristicas
			* Evaluación perezosa
				* Solamente se evalúan las ecuaciones, se calcula\n o se computa aquello qué es estrictamente necesario\n para hacer cálculos
				* Podemos definir tipos de datos infinitos
				* Podemos definir la serie de Fibonacci
			* Asistencia de funciones de orden Superior
			* Evaluación perezosa
	*[#Green] Programacion Logica
		*[#Orange] Se acuda la lógica de predicados de primer orden
			* Predicados 
				* Son relaciones entre objetos
				* Este tipo de relaciones no establece un orden\n entre argumentos de entrada y argumentos de salida
		*[#Orange] Ventaja
			* Nos permite ser más declarativos
				* Ejemplo
					* Podemos definir una relación factorial entre dos números\n y nos dice si uno de los dos números es el factorial del otro
		*[#Orange] Forma de realizar computos
			* El intérprete dado un conjunto de relaciones y\n predicados opera mediante algoritmos de resolución
			* Intento demostrar predicados dentro de este sistema
				* Ejemplo
					* Si el factorial de 2 es i, sí le damos i como\n una variable lo que hace el intérprete es\n encontrar cuáles son los valores para la variable i\n que cumplen esa relación de factorial
	*[#Green] Programacion orientada\n a inteligencia artificial
		*[#Orange] Lisp
			* Es un lenguaje que tiene relación\n con la programación funcional
			* Se estudia como un modelo de programación imperativa
			* Es un tipo de lenguaje híbrido
				* Permite hacer un tipo de programación imperativa
		*[#Orange] Prolog
			* Ese lenguaje estándar en el que se implementa\n el concepto de programación lógica






@endmindmap
```
# TEMA: "Lenguaje de Programación Funcional (2015)"
```plantuml
@startmindmap
*[#lightblue] Lenguaje de Programación Funcional
	*[#Green] Paradigmas de programacion
		*[#Orange] Son el modelo de la computación en el que los\n diferentes lenguajes dotan de semántica a los programas
		*[#Orange] Estaban basados en el modelo de computacion de von neumann
			* Lenguaje imperativo
			* Los programas deben almacenarse en la\n misma máquina antes de ser ejecutados
			* Serie de instrucciones ejecutándose\n de secuencialmente
				* Según el estado del cómputo
				* Modificaciones mediante instrucción de asignación
	*[#Green] Variantes
		*[#Orange] Programación orientada objetos
			* Pequeños fragmentos de código que van interactuando entre sí
			* Se compone el instrucciones que se Ejecutan secuencialmente
		*[#Orange] Logica Simbolica
			* Paradigma de programación lógico
				* Conjunto de Sentencia que Define lo que es verdad\n y lo que es conocido para un programa
	*[#Green] Landa calculo
		*[#Orange] Se desarrolló en la década de 1930
			* Desarrollado por
				* Church
				* Kleene 
		*[#Orange] Se pensó como un sistema
			* Para estudiar el concepto de función y recursividad
			* Sistema potente equivalente al sistema de von neumann
		*[#Orange] Peter Landin (1965)
			* Desarrollo los cimientos para un lenguaje de programación
	*[#Green] Bases de la programacion
		*[#Orange] Es una colección de datos,\ninstrucciones que operan sobre dichos datos
		*[#Orange] Las instrucciones se ejecutan en un orden adecuado
			* Pueden modifican según el valor de las variables
	*[#Green] Programacion funcional
		*[#Orange] Son solamente funciones matemáticas
			* Reciben parámetros
			* Devuelven salidas
		*[#Orange] Concencuencias
			* Asignacion
			* Bucles
				* Serie de instrucciones que se repiten un número indeterminado\n de veces y están controladas por el estado del computo 
					* Son resueltos únicamente mediante la recursión
		*[#Orange] Constante
			* Son equiparables a las funciones
		*[#Orange] Ventajas
			* solo depender de los parámetros de entrada
			* Transparencia referencial
			* Los resultados son independientes del orden\n en que se realizan los cálculos
		*[#Orange] Caracteristicas
			* Todo gira alrededor de las funciones
			* Pueden ser parámetros incluso de otras funciones
		*[#Orange] Evaluacion
			* Peresoza
				* Evaluar las expresiones desde adentro hacia fuera
				* Evaluacion impaciente o estricta
			* Inconveniente
				* Si no se puede evaluar una expresión en tiempo finito
					* el cálculo se interrumpe o produce error
			* Evaluacion no estrica
				* Evalúa desde afuera hacia adentro
				* Es mas complicada de implementar
	*[#Green] Entorno multiprocesador
		*[#Orange] El resultado de las funciones no dependen de las otras
			* se pueden paralizar sin problemas














@endmindmap